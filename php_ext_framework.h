/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_EXT_FRAMEWORK_H
#define PHP_EXT_FRAMEWORK_H

extern zend_module_entry ext_framework_module_entry;
#define phpext_ext_framework_ptr &ext_framework_module_entry

#ifdef PHP_WIN32
#	define PHP_EXT_FRAMEWORK_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_EXT_FRAMEWORK_API __attribute__ ((visibility("default")))
#else
#	define PHP_EXT_FRAMEWORK_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

#ifndef PHP_FE_END
#define PHP_FE_END { NULL, NULL, NULL }
#endif

#define FETCH_THIS                    Z_OBJCE_P(getThis()), getThis()
#define EXT_STARTUP_FUNCTION(module)  ZEND_MINIT_FUNCTION(ext_##module)
#define EXT_STARTUP(module)           ZEND_MODULE_STARTUP_N(ext_##module)(INIT_FUNC_ARGS_PASSTHRU)

PHP_MINIT_FUNCTION(ext_framework);
PHP_MSHUTDOWN_FUNCTION(ext_framework);
PHP_RINIT_FUNCTION(ext_framework);
PHP_RSHUTDOWN_FUNCTION(ext_framework);
PHP_MINFO_FUNCTION(ext_framework);

PHP_FUNCTION(confirm_ext_framework_compiled);	/* For testing, remove later. */

/* 
  	Declare any global variables you may need between the BEGIN
	and END macros here:     

ZEND_BEGIN_MODULE_GLOBALS(ext_framework)
	long  global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(ext_framework)
*/

#ifdef ZTS
#define EXT_FRAMEWORK_G(v) TSRMG(ext_framework_globals_id, zend_ext_framework_globals *, v)
#else
#define EXT_FRAMEWORK_G(v) (ext_framework_globals.v)
#endif

#endif	/* PHP_EXT_FRAMEWORK_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
