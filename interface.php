<?php
interface ext_view_impl{
	/**
	 * 设置模板标签左边界符
	 * @param string $delim 符号
	 */
	public function setLeftDelim($delim);

	/**
	 * 获取模板标签左边界符
	 * @return string 左边界符
	 */
	public function getLeftDelim();

	/**
	 * 设置模板标签右边界符
	 * @param string $delim 符号
	 */
	public function setRgihtDelim($delim);

	/**
	 * 获取模板标签右边界符
	 * @return string 右边界符
	 */
	public function getRgihtDelim();

	/**
	 * 设置模板根路径
	 * @param string $dir 路径
	 */
	public function setTemplateDir($dir);

	/**
	 * 获取模板根路径
	 * @return string 路径
	 */
	public function getTemplateDir();

	/**
	 * 设置模板编译后存放路径
	 * @param string $dir 路径
	 */
	public function setCompileDir($dir);

	/**
	 * 获取模板编译后存放路径
	 * @return string 路径
	 */
	public function getCompliteDir();

	/**
	 * 设置模板缓存路径
	 * @param string $dir 路径
	 */
	public function setCacheDir($dir);

	/**
	 * 获取模板缓存路径
	 * @return string 路径
	 */
	public function getCacheDir();

	/**
	 * 设置全局缓存时间
	 * @param int $time 时间 单位秒
	 */
	public function setCacheTime($time);

	/**
	 * 获取全局缓存时间
	 * @return int 时间
	 */
	public function getCacheTime();

	/**
	 * 设置是否开启全局缓存
	 * @param  int  $cache 是否开启全局缓存
	 */
	public function setOpenCache($cache);

	/**
	 * 获取是否已经开启全局缓存
	 * @return int >0表示开启
	 */
	public function getOpenCache();

	public function cacheHandler();

	public function checkCache($template);

	public function templateParserHandler($content);

	/**
	 * 赋值
	 * @param  string $key   键名
	 * @param  mixed  $value  任意
	 */
	public function assing($key, $value);

	/**
	 * 输出内容
	 * @param  string   $template   模板文件
	 * @param  int  	$cache_id   缓存ID
	 * @param  int  	$compile_id 编译ID
	 */
	public function display($template, $cache_id = 0, $compile_id = 0);

	/**
	 * 获取输出内容
	 * @param  string   $template   模板文件
	 * @param  int  	$cache_id   缓存ID
	 * @param  int  	$compile_id 编译ID
	 * @return string  编译执行后的输出内容
	 */
	public function fetch($template, $cache_id = 0, $compile_id = 0);

	/**
	 * 如果模板存在有效的缓存则返回真
	 * @param  string   $template   模板文件
	 * @param  int  	$cache_id   缓存ID
	 * @param  int  	$compile_id 编译ID
	 * @return boolean
	 */
	public function isCached($template, $cache_id = 0, $compile_id = 0)
}