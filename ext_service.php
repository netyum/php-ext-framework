<?php
interface ext_service_filter{
	public function filter($class_name, $params, $result=false);
}

class ext_service{
	public $after;
	public $before;

	public function before_filter(ext_service_filter $before_filter, 
									$class_name=false, 
									$params=false);

	public function create($class_name, $params=false);

	public function after_filter(ext_service_filter $after_filter, 
									$class_name=false, 
									$params=false, 
									$result=false);
}