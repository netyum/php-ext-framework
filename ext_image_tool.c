/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id: ext_image_tool.c */
/* 图片处理工具ext_image_tool类 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "main/SAPI.h"  
#include "Zend/zend_interfaces.h"  
#include "ext/standard/php_var.h"  
#include "ext/standard/php_string.h"  
#include "ext/standard/php_smart_str.h"  
#include "ext/standard/url.h"
#include <wand/MagickWand.h>
#include <string.h>
#include "php_ext_framework.h"
#include "ext_image_tool.h"

#ifndef PHP_WIN32
  #define stricmp strcasecmp
#endif

/* If you declare any globals in php_ext_image_tool.h uncomment this:
ZEND_DECLARE_MODULE_GLOBALS(ext_image_tool)
*/ 
zend_class_entry *ext_image_tool_ce;

/* True global resources - no need for thread safety here */
static int le_ext_image_tool, le_image_wand/*ImageMagick::MagickWand句柄*/, le_image_pixel/*ImageMagick::PixelWand句柄*/;

static void destroy_magick_wand(zend_rsrc_list_entry *rsrc TSRMLS_DC){
	MagickWand *magick_wand = (MagickWand*)rsrc->ptr;

	DestroyMagickWand(magick_wand);
}

static void destroy_pixel_wand(zend_rsrc_list_entry *rsrc TSRMLS_DC){
	PixelWand *pixel_wand = (PixelWand*)rsrc->ptr;

	DestroyPixelWand(pixel_wand);
}

ZEND_BEGIN_ARG_INFO_EX(void_arginfo, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(setData_arginfo, 0, 0, 0)
	ZEND_ARG_INFO(0, img_data)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(annotate_arginfo, 0, 0, 3)
	ZEND_ARG_INFO(0, txt)
	ZEND_ARG_INFO(0, opacity)
	ZEND_ARG_INFO(0, gravity)
	ZEND_ARG_INFO(0, font)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(composite_arginfo, 0, 0, 3)
	ZEND_ARG_INFO(0, width)
	ZEND_ARG_INFO(0, height)
	ZEND_ARG_INFO(0, color)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(exec_arginfo, 0, 0, 1)
	ZEND_ARG_INFO(0, format)
	ZEND_ARG_INFO(0, display)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(resize_arginfo, 0, 0, 2)
	ZEND_ARG_INFO(0, width)
	ZEND_ARG_INFO(0, height)
	ZEND_ARG_INFO(0, thumbnail)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(resizeRatio_arginfo, 0, 0, 1)
	ZEND_ARG_INFO(0, ratio)
	ZEND_ARG_INFO(0, thumbnail)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(rotate_arginfo, 0, 0, 1)
	ZEND_ARG_INFO(0, degree)
ZEND_END_ARG_INFO()

//ext_image_tool::__construct($img_data="");
ZEND_METHOD(ext_image_tool, __construct){
	zval *img_data=NULL, *z_magick_wand;
	MagickWand *magick_wand;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "|z", &img_data) == FAILURE){
		RETURN_FALSE;
	}
	
	MagickWandGenesis();
	magick_wand = NewMagickWand();
	z_magick_wand = zend_read_property(FETCH_THIS, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);
	MAKE_STD_ZVAL(z_magick_wand);
	ZEND_REGISTER_RESOURCE(z_magick_wand, magick_wand, le_image_wand);
	zend_update_property(FETCH_THIS, ZEND_STRL("magick_wand"), z_magick_wand TSRMLS_CC);

	if(img_data != NULL && Z_TYPE_P(img_data) == IS_STRING){
		if( MagickReadImage(magick_wand, Z_STRVAL_P(img_data)) == MagickFalse){
			zend_error(E_WARNING, "img filepath not exists!");
			RETURN_FALSE;
		}
	}

	RETURN_TRUE;
}

ZEND_METHOD(ext_image_tool, __tostring){
	zval *z_magick_wand;
	MagickWand *magick_wand;
	size_t img_size;
	unsigned char *img_data;

	z_magick_wand = zend_read_property(FETCH_THIS, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);
	ZEND_FETCH_RESOURCE_NO_RETURN(magick_wand, MagickWand*, &z_magick_wand, -1, IMAGETOOL_MAGICKWAND_RSRC_NAME, le_image_wand);

	img_data = MagickGetImageBlob(magick_wand, &img_size);
	ZVAL_STRINGL(return_value, img_data, img_size, 1);
	MagickRelinquishMemory(img_data);
}

//ext_image_tool::annotate($txt, $opacity, $gravity, array $font);
ZEND_METHOD(ext_image_tool, annotate){
	zval *txt, *opacity, *gravity, *font;
	zval *z_magick_wand;
	MagickWand *magick_wand;
	DrawingWand *drawing_wand;
	int img_width, img_height;
	float x, y;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "zzzz!", &txt, &opacity, &gravity, &font) == FAILURE){
		RETURN_FALSE;
	}

	if(Z_TYPE_P(txt) != IS_STRING){
		zend_error(E_WARNING, "txt must be string.");
		RETURN_FALSE;
	}

	if(Z_TYPE_P(opacity) != IS_DOUBLE){
		zend_error(E_WARNING, "opacity must be float.");
		RETURN_FALSE;
	}

	if(Z_TYPE_P(gravity) != IS_LONG){
		zend_error(E_WARNING, "gravity must be int.");
		RETURN_FALSE;
	}

	if(Z_TYPE_P(font) != IS_ARRAY){
		zend_error(E_WARNING, "font must be array.");
		RETURN_FALSE;
	}

	z_magick_wand = zend_read_property(FETCH_THIS, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);
	drawing_wand = NewDrawingWand();
	DrawSetTextEncoding(drawing_wand, "UTF-8");
	DrawSetFillOpacity(drawing_wand, Z_DVAL_P(opacity));
	DrawSetTextAlignment(drawing_wand, CenterAlign);
	ZEND_FETCH_RESOURCE_NO_RETURN(magick_wand, MagickWand*, &z_magick_wand, -1, IMAGETOOL_MAGICKWAND_RSRC_NAME, le_image_wand);
	img_height = MagickGetImageHeight(magick_wand);
	img_width= MagickGetImageWidth(magick_wand);

	switch(Z_LVAL_P(gravity)){
		case IMAGETOOL_NORTHWEST:
			DrawSetTextAlignment(drawing_wand, LeftAlign);
			x = 0.0;
			y = 20.0;
		break;
		case IMAGETOOL_NORTH:
			x = img_width/2;
			y = 20.0;
		break;
		case IMAGETOOL_NORTHEAST:
			DrawSetTextAlignment(drawing_wand, RightAlign);
			x = img_width;
			y = 20.0;
		break;
		case IMAGETOOL_WEST:
			DrawSetTextAlignment(drawing_wand, LeftAlign);
			x = 0.0;
			y = img_height/2;
		break;
		case IMAGETOOL_CENTER:
			x = img_width/2;
			y = img_height/2;
		break;
		case IMAGETOOL_EAST:
			DrawSetTextAlignment(drawing_wand, RightAlign);
			x = img_width;
			y = img_height/2;
		break;
		case IMAGETOOL_SOUTHWEST:
			DrawSetTextAlignment(drawing_wand, LeftAlign);
			x = 0.0;
			y = img_height;
		break;
		case IMAGETOOL_SOUTH:
			x = img_width/2;
			y = img_height;
		break;
		case IMAGETOOL_SOUTHEAST:
			DrawSetTextAlignment(drawing_wand, RightAlign);
			x = img_width;
			y = img_height;
		break;
		case IMAGETOOL_STATIC:
		default:
			x = 0.0;
			y = 20.0;
	}

	if(font != NULL){
		for(zend_hash_internal_pointer_reset(Z_ARRVAL_P(font));
			zend_hash_has_more_elements(Z_ARRVAL_P(font)) == SUCCESS;
			zend_hash_move_forward(Z_ARRVAL_P(font))){
			char *key;
			uint key_len;
			ulong idx;
			zval **pp_zval;

			if(zend_hash_get_current_key_ex(Z_ARRVAL_P(font), &key, &key_len, &idx, 0, NULL) != HASH_KEY_IS_STRING){
				continue;
			}

			if(zend_hash_get_current_data(Z_ARRVAL_P(font), (void**)&pp_zval) == FAILURE){
				continue;
			}

			if(stricmp(key, "color") == 0){
				PixelWand *pixel_wand;
				pixel_wand = NewPixelWand();
				convert_to_string(*pp_zval);

				PixelSetColor(pixel_wand, Z_STRVAL_PP(pp_zval));
				DrawSetFillColor(drawing_wand, pixel_wand);
			}else if(stricmp(key, "name") == 0){
				convert_to_string(*pp_zval);
				DrawSetFont(drawing_wand, Z_STRVAL_PP(pp_zval));
			}else if(stricmp(key, "size") == 0){
				convert_to_long(*pp_zval);
				DrawSetFontSize(drawing_wand, Z_LVAL_PP(pp_zval));
			}else if(stricmp(key, "weight") == 0){
				convert_to_long(*pp_zval);
				DrawSetFontWeight(drawing_wand, Z_LVAL_PP(pp_zval));
			}
		}
	}
	// php_printf("width=%d,height=%d,x=%f,y=%f\n", img_width, img_height, x, y);
	DrawAnnotation(drawing_wand, x, y, Z_STRVAL_P(txt));
	MagickDrawImage(magick_wand, drawing_wand);
}

ZEND_METHOD(ext_image_tool, clean){
	zval *z_magick_wand;
	MagickWand *magick_wand;

	GET_MAGICK_WAND(z_magick_wand, magick_wand);
	ClearMagickWand(magick_wand);
	RETURN_TRUE;
}


//ext_image_tool::composite($src_img, $x, $y)
ZEND_METHOD(ext_image_tool, composite){
	zval *z_magick_wand, *z_x, *z_y;
	zval *z_src_magcik_wand, *src_img;
	MagickWand *magick_wand, *src_magcik_wand;
	MagickSizeType img_size;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "zzz", &src_img, &z_x, &z_y) == FAILURE){
		RETURN_FALSE;
	}

	GET_MAGICK_WAND(z_magick_wand, magick_wand);
	convert_to_long(z_x);
	convert_to_long(z_y);

	if(Z_TYPE_P(src_img) == IS_OBJECT && instanceof_function(Z_OBJCE_P(src_img), ext_image_tool_ce)){
		z_src_magcik_wand = zend_read_property(Z_OBJCE_P(src_img), src_img, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);
		src_magcik_wand = ZEND_FETCH_RESOURCE_NO_RETURN(src_magcik_wand, MagickWand*, &z_src_magcik_wand, -1, IMAGETOOL_MAGICKWAND_RSRC_NAME, le_image_wand);
		if(MagickGetImageLength(src_magcik_wand, &img_size) == MagickFalse) {
			RETURN_FALSE;
		}
	}else if(Z_TYPE_P(src_img) == IS_STRING){
		src_magcik_wand = NewMagickWand();
		if(MagickReadImageBlob(src_magcik_wand, (void*)Z_STRVAL_P(src_img), Z_STRLEN_P(src_img)) == MagickFalse){
			RETURN_FALSE;
		}
	}else{
		RETURN_FALSE;
	}

	if(MagickCompositeImage(magick_wand, src_magcik_wand, OverCompositeOp, Z_LVAL_P(z_x), Z_LVAL_P(z_y)) == MagickFalse){
		RETURN_FALSE;
	}

	RETURN_TRUE;
}

ZEND_METHOD(ext_image_tool, errmsg){
	zval *errmsg;
	errmsg = zend_read_property(FETCH_THIS, ZEND_STRL("errmsg"), 0 TSRMLS_CC);

	if(Z_TYPE_P(errmsg) == IS_NULL){
		RETURN_NULL();
	}
	else{
		RETURN_STRINGL(Z_STRVAL_P(errmsg), Z_STRLEN_P(errmsg), 1);
	}
}

ZEND_METHOD(ext_image_tool, errcode){
	zval *errcode;
	errcode = zend_read_property(FETCH_THIS, ZEND_STRL("errcode"), 0 TSRMLS_CC);

	RETURN_LONG(Z_LVAL_P(errcode));
}

//ext_image_tool::exec($filename, $display=false);
ZEND_METHOD(ext_image_tool, exec){
	zval *filename, *display = NULL, *z_magick_wand;
	MagickWand *magick_wand;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z|z", &filename, &display) == FAILURE){
		RETURN_FALSE;
	}

	z_magick_wand = zend_read_property(FETCH_THIS, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);
	ZEND_FETCH_RESOURCE_NO_RETURN(magick_wand, MagickWand*, &z_magick_wand, -1, IMAGETOOL_MAGICKWAND_RSRC_NAME, le_image_wand);

	if(display == NULL){
		MAKE_STD_ZVAL(display);
		ZVAL_LONG(display, 0);
	}
	convert_to_boolean(display);
	if(Z_BVAL_P(display) <= 0){
		if(MagickWriteImage(magick_wand, Z_STRVAL_P(filename)) == MagickFalse){
			zend_error(E_WARNING, "write image error.");
			RETURN_FALSE;
		}
	}else{
		if(MagickWriteImage(magick_wand, Z_STRVAL_P(filename)) == MagickFalse){
			zend_error(E_WARNING, "write image error.");
			RETURN_FALSE;
		}
	}
}

//ext_image_tool::flipH()
ZEND_METHOD(ext_image_tool, flipH){
	zval *function_name, *params[1];
	MAKE_STD_ZVAL(function_name);
	ZVAL_STRINGL(function_name, "rotate", 6, 1);

	MAKE_STD_ZVAL(params[0]);
	ZVAL_LONG(params[0], 180);

	call_user_function(NULL, &this_ptr, function_name, return_value, 1, params TSRMLS_CC);
	zval_dtor(function_name);
	zval_dtor(params[0]);
	RETURN_TRUE;
}

//ext_image_tool::flipV()
ZEND_METHOD(ext_image_tool, flipV){
	zval *function_name, *params[1];
	MAKE_STD_ZVAL(function_name);
	ZVAL_STRINGL(function_name, "rotate", 6, 1);

	MAKE_STD_ZVAL(params[0]);
	ZVAL_LONG(params[0], 90);

	call_user_function(NULL, &this_ptr, function_name, return_value, 1, params TSRMLS_CC);
	zval_dtor(function_name);
	zval_dtor(params[0]);
	RETURN_TRUE;
}

//ext_image_tool::getImageAttr()
ZEND_METHOD(ext_image_tool, getImageAttr){
	zval *z_magick_wand;
	size_t width, height;
	MagickWand *magick_wand;
	char *type;

	GET_MAGICK_WAND(z_magick_wand, magick_wand);

	width = MagickGetImageWidth(magick_wand);
	height= MagickGetImageHeight(magick_wand);
	type= MagickGetImageFormat(magick_wand);

	array_init(return_value);
	add_assoc_long(return_value, "width", width);
	add_assoc_long(return_value, "height", height);
	add_assoc_stringl(return_value, "type", type, strlen(type), 1);
}

//ext_image_tool::getImageAttr()
ZEND_METHOD(ext_image_tool, improve){
	zval *z_magick_wand;
	MagickWand *magick_wand;

	GET_MAGICK_WAND(z_magick_wand, magick_wand);

	MagickEnhanceImage(magick_wand);

	RETURN_TRUE;
}

//ext_image_tool::resize($width=0, $height=0, $thumbnail=true);
ZEND_METHOD(ext_image_tool, resize){
	zval *z_magick_wand, *width, *height, *thumbnail=NULL;
	size_t img_width, img_height;
	MagickWand *magick_wand;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "zz|l", &width, &height, &thumbnail) == FAILURE){
		RETURN_FALSE;
	}

	convert_to_long(width);
	convert_to_long(height);
	if(thumbnail == NULL){
		MAKE_STD_ZVAL(thumbnail);
		ZVAL_LONG(thumbnail, 1);
	}else{
		convert_to_long(thumbnail);
	}

	GET_MAGICK_WAND(z_magick_wand, magick_wand);

	img_width = MagickGetImageWidth(magick_wand);
	img_height= MagickGetImageHeight(magick_wand);
	Z_LVAL_P(width) = (Z_LVAL_P(width) > img_width) ? img_width : Z_LVAL_P(width);
	Z_LVAL_P(height) = (Z_LVAL_P(height) > img_height) ? img_height : Z_LVAL_P(height);

	if(Z_LVAL_P(thumbnail) > 0){
		MagickThumbnailImage(magick_wand, Z_LVAL_P(width), Z_LVAL_P(height));
	}else{
		MagickResizeImage(magick_wand, Z_LVAL_P(width), Z_LVAL_P(height), LanczosFilter, 1.0);
	}

	RETURN_TRUE;
}

//ext_image_tool::resizeRatio($ratio, $thumbnail=true)
ZEND_METHOD(ext_image_tool, resizeRatio){
	zval *z_magick_wand, *ratio, *thumbnail = NULL;
	MagickWand *magick_wand;
	size_t src_width, src_height, dst_width, dst_height;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z|z", &ratio, &thumbnail) == FAILURE){
		RETURN_FALSE;
	}

	GET_MAGICK_WAND(z_magick_wand, magick_wand);

	convert_to_double(ratio);

	src_width = MagickGetImageWidth(magick_wand);
	src_height= MagickGetImageHeight(magick_wand);

	if(Z_DVAL_P(ratio) > 1.0) Z_DVAL_P(ratio) = 1.0;
	if(Z_DVAL_P(ratio) < 0.0) Z_DVAL_P(ratio) = 0.0;

	dst_width = src_width * Z_DVAL_P(ratio);
	dst_height= src_height* Z_DVAL_P(ratio); 

	if(thumbnail == NULL){
		MAKE_STD_ZVAL(thumbnail);
		ZVAL_LONG(thumbnail, 1);
	}

	if(Z_LVAL_P(thumbnail) > 0){
		MagickThumbnailImage(magick_wand, dst_width, dst_height);
	}else{
		MagickResizeImage(magick_wand, dst_width, dst_height, LanczosFilter, 1.0);
	}

	RETURN_TRUE;
}

//ext_image_tool:rotate($degree=90);
ZEND_METHOD(ext_image_tool, rotate){
	zval *z_magick_wand, *degree;
	MagickWand *magick_wand;
	PixelWand *pixel_wand;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "|z!", degree) == FAILURE){
		RETURN_FALSE;
	}

	if(Z_TYPE_P(degree) == IS_NULL){
		MAKE_STD_ZVAL(degree);
		ZVAL_LONG(degree, 90);
	}

	if(Z_TYPE_P(degree) != IS_LONG) convert_to_long(degree);

	pixel_wand = NewPixelWand();
	z_magick_wand = zend_read_property(FETCH_THIS, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);
	ZEND_FETCH_RESOURCE_NO_RETURN(magick_wand, MagickWand *, &z_magick_wand, -1, IMAGETOOL_MAGICKWAND_RSRC_NAME, le_image_wand);

	MagickRotateImage(magick_wand, pixel_wand, Z_LVAL_P(degree));
}

//ImageTool::setData($img_blob)
ZEND_METHOD(ext_image_tool, setData){
	zval *z_magick_wand, *z_img_blob;
	MagickWand *magick_wand;

	if(zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z", &z_img_blob) == FAILURE){
		RETURN_FALSE;
	}

	if(Z_TYPE_P(z_img_blob) != IS_STRING){
		RETURN_FALSE;
	}

	GET_MAGICK_WAND(z_magick_wand, magick_wand);

	if(MagickReadImageBlob(magick_wand, (void*)Z_STRVAL_P(z_img_blob), Z_STRLEN_P(z_img_blob)) == MagickTrue){
		RETURN_TRUE;
	}else{
		RETURN_FALSE;
	}
}

static zend_function_entry ext_image_tool_methods[] = {
	ZEND_ME(ext_image_tool, __construct, setData_arginfo, ZEND_ACC_CTOR|ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, __tostring, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, annotate, annotate_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, clean, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, composite, composite_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, errmsg, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, errcode, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, exec, exec_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, flipH, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, flipV, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, getImageAttr, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, improve, void_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, resize, resize_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, resizeRatio, resizeRatio_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, rotate, rotate_arginfo, ZEND_ACC_PUBLIC)
	ZEND_ME(ext_image_tool, setData, setData_arginfo, ZEND_ACC_PUBLIC)
	{NULL, NULL, NULL}
};


/* {{{ PHP_MINIT_FUNCTION
 */
EXT_STARTUP_FUNCTION(image_tool)
{
	/* If you have INI entries, uncomment these lines 
	REGISTER_INI_ENTRIES();
	*/
	zend_class_entry image_tool_ce;
	INIT_CLASS_ENTRY(image_tool_ce, "ext_image_tool", ext_image_tool_methods);
	ext_image_tool_ce = zend_register_internal_class(&image_tool_ce TSRMLS_CC);

	zend_declare_property_null(ext_image_tool_ce, ZEND_STRL("magick_wand"), ZEND_ACC_PROTECTED TSRMLS_CC);
	zend_declare_property_long(ext_image_tool_ce, ZEND_STRL("errcode"), 0, ZEND_ACC_PROTECTED TSRMLS_CC);
	zend_declare_property_null(ext_image_tool_ce, ZEND_STRL("errmsg"), ZEND_ACC_PROTECTED TSRMLS_CC);

	REGISTER_LONG_CONSTANT("IMAGETOOL_NORTHWEST", IMAGETOOL_NORTHWEST, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_NORTH", IMAGETOOL_NORTH, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_NORTHEAST", IMAGETOOL_NORTHEAST, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_WEST", IMAGETOOL_WEST, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_CENTER", IMAGETOOL_CENTER, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_EAST", IMAGETOOL_EAST, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_SOUTHWEST", IMAGETOOL_SOUTHWEST, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_SOUTH", IMAGETOOL_SOUTH, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_SOUTHEAST", IMAGETOOL_SOUTHEAST, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_STATIC", IMAGETOOL_STATIC, CONST_CS);

	REGISTER_LONG_CONSTANT("IMAGETOOL_TOP_LEFT", IMAGETOOL_TOP_LEFT, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_TOP_CENTER", IMAGETOOL_TOP_CENTER, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_TOP_RIGHT", IMAGETOOL_TOP_RIGHT, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_CENTER_LEFT", IMAGETOOL_CENTER_LEFT, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_CENTER_CENTER", IMAGETOOL_CENTER_CENTER, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_CENTER_RIGHT", IMAGETOOL_CENTER_RIGHT, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_BOTTOM_LEFT", IMAGETOOL_BOTTOM_LEFT, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_BOTTOM_CENTER", IMAGETOOL_BOTTOM_CENTER, CONST_CS);
	REGISTER_LONG_CONSTANT("IMAGETOOL_BOTTOM_RIGHT", IMAGETOOL_BOTTOM_RIGHT, CONST_CS);

	//注册MagickWand、PixelWand资源
	le_image_wand = zend_register_list_destructors_ex(destroy_magick_wand, NULL, IMAGETOOL_MAGICKWAND_RSRC_NAME, module_number);
	le_image_pixel= zend_register_list_destructors_ex(destroy_pixel_wand, NULL, IMAGETOOL_PIXELWAND_RSRC_NAME, module_number);

	return SUCCESS;
}
/* }}} */

