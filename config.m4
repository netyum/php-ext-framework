PHP_ARG_WITH(ext_framework, for ext_framework support,
dnl Make sure that the comment is aligned:
[  --with-ext_framework             Include ext_framework support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(ext_framework, whether to enable ext_framework support,
dnl Make sure that the comment is aligned:
[  --enable-ext_framework           Enable ext_framework support])

SEARCH_PATH="/usr/local/include /usr/include /usr/local/include/ImageMagick-6"
LIB_SEARCH_PATH="/usr/lib /usr/local/lib"

if test "$PHP_EXT_FRAMEWORK" != "no"; then

  dnl 验证curl头文件
  for i in $SEARCH_PATH ; do
    if test -f $i/curl/curl.h; then
      CURL_DIR=$i
    fi
  done

  if test -z "$CURL_DIR"; then
    AC_MSG_ERROR([curl.h not found])
  fi


  dnl 验证libcurl.so文件
  for i in $LIB_SEARCH_PATH ; do
    if test -f $i/libcurl.so; then
      LIB_CURL_DIR=$i
    fi
  done

  if test -z "$LIB_CURL_DIR"; then
    AC_MSG_ERROR([libcurl.so not found])
  fi

  PHP_ADD_INCLUDE($CURL_DIR)
  PHP_ADD_LIBRARY_WITH_PATH(curl, $LIB_CURL_DIR, EXT_FRAMEWORK_SHARED_LIBADD)

  dnl 验证imageMagick头文件
  for i in $SEARCH_PATH ; do
    if test -f $i/wand/MagickWand.h; then
      IMAGEMAGICK_DIR=$i
    fi
  done

  if test -z "$IMAGEMAGICK_DIR"; then
    AC_MSG_ERROR([/wand/MagickWand.h not found])
  fi

  IMAGICK_LIBS=`MagickWand-config --libs`
  IMAGICK_INCS=`MagickWand-config --cflags`
      
  PHP_EVAL_LIBLINE($IMAGICK_LIBS, EXT_FRAMEWORK_SHARED_LIBADD)
  PHP_EVAL_INCLINE($IMAGICK_INCS)
  PHP_SUBST(EXT_FRAMEWORK_SHARED_LIBADD)

  PHP_NEW_EXTENSION(ext_framework, ext_framework.c ext_view.c ext_fetch_url.c ext_image_tool.c, $ext_shared)
fi