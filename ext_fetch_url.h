/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

#ifndef EXT_FETCH_URL_H
#define EXT_FETCH_URL_H

#ifdef PHP_WIN32
  #define FETCH_CURL_MODE CURL_GLOBAL_WIN32
#else
  #define FETCH_CURL_MODE CURL_GLOBAL_ALL
#endif
  
#define FETCH_CLASS_NAME  "ext_fetch_url"
#define FETCH_CLASS_CE    g_fetch_ce
#define FETCH_THIS        Z_OBJCE_P(getThis()), getThis()
#define FETCH_ERROR(errmsg, errno) zend_update_property_stringl(FETCH_THIS, ZEND_STRL("errmsg"), errmsg, sizeof(errmsg)-1 TSRMLS_CC);\
                                    zend_update_property_long(FETCH_THIS, ZEND_STRL("errno"), errno TSRMLS_CC)

extern zend_class_entry *ext_fetch_url_ce;

EXT_STARTUP_FUNCTION(fetch_url);
#endif

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
