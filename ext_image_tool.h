/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2012 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_IMAGE_TOOL_H
#define PHP_IMAGE_TOOL_H

extern zend_class_entry *ext_image_tool_ce;

EXT_STARTUP_FUNCTION(image_tool);

#define FETCH_THIS Z_OBJCE_P(getThis()), getThis()
#define IMAGETOOL_MAGICKWAND_RSRC_NAME "MagickWand"
#define IMAGETOOL_PIXELWAND_RSRC_NAME "PixelWand"

#define IMAGETOOL_NORTHWEST 1
#define IMAGETOOL_NORTH     2
#define IMAGETOOL_NORTHEAST 3
#define IMAGETOOL_WEST      4
#define IMAGETOOL_CENTER    5
#define IMAGETOOL_EAST      6
#define IMAGETOOL_SOUTHWEST 7
#define IMAGETOOL_SOUTH     8
#define IMAGETOOL_SOUTHEAST 9
#define IMAGETOOL_STATIC    10

#define IMAGETOOL_TOP_LEFT      1
#define IMAGETOOL_TOP_CENTER    2
#define IMAGETOOL_TOP_RIGHT     3
#define IMAGETOOL_CENTER_LEFT   4
#define IMAGETOOL_CENTER_CENTER 5
#define IMAGETOOL_CENTER_RIGHT  6
#define IMAGETOOL_BOTTOM_LEFT   7
#define IMAGETOOL_BOTTOM_CENTER 8
#define IMAGETOOL_BOTTOM_RIGHT  9

#define GET_MAGICK_WAND(zval, magick_wand) zval = zend_read_property(FETCH_THIS, ZEND_STRL("magick_wand"), 0 TSRMLS_CC);\
        ZEND_FETCH_RESOURCE_NO_RETURN(magick_wand, MagickWand*, &zval, -1, IMAGETOOL_MAGICKWAND_RSRC_NAME, le_image_wand);


#ifdef ZTS
#define IMAGE_TOOL_G(v) TSRMG(image_tool_globals_id, zend_image_tool_globals *, v)
#else
#define IMAGE_TOOL_G(v) (image_tool_globals.v)
#endif

#endif